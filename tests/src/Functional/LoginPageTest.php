<?php

namespace Drupal\Tests\prevent_unsafe_login\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Simple browser test.
 *
 * @group prevent_unsafe_login
 */
class LoginPageTest extends BrowserTestBase {

  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'prevent_unsafe_login',
  ];

  /**
   * Tests that login form is disabled, Assuming test bot uses http.
   */
  public function testLoginFormIsDisabled() {
    $this->drupalGet('user/login');
    $webAssert = $this->assertSession();
    $webAssert->statusCodeEquals(200);
    $webAssert->fieldDisabled('edit-name');
    $webAssert->fieldDisabled('edit-pass');
    // Testing button disabled is a bit more complex.
    $submitButton = $webAssert->buttonExists('edit-submit');
    $this->assertTrue($submitButton->hasAttribute('disabled'));

    // Ensure that the test is not marked as risky because of no assertions.
    // see https://gitlab.com/weitzman/drupal-test-traits/-/commit/82bf5059908f9073b3468cb7313960da72176d9a
    $this->addToAssertionCount(1);
  }

}
